import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Layout1Component } from './views/layout1/layout1.component';
import { Layout2Component } from './views/layout2/layout2.component';

@NgModule({
  declarations: [
    AppComponent,
    Layout1Component,
    Layout2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
